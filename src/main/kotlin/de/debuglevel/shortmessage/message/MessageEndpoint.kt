package de.debuglevel.shortmessage.message

import de.debuglevel.shortmessage.providers.MessageReceipt
import de.debuglevel.shortmessage.providers.ShortmessageSenderService
import jakarta.inject.Singleton
import mu.KotlinLogging

// TODO: Does not yet implement any security
@Singleton
class MessageEndpoint(
    private val shortmessageSenderService: ShortmessageSenderService
) : MessageGrpcKt.MessageCoroutineImplBase() {
    private val logger = KotlinLogging.logger {}

    override suspend fun send(request: SendRequest): SendReply {
        logger.debug("Called send($request)")

        val messageReceipt = shortmessageSenderService.send(request.recipientNumber, request.body)
        val sendReply = messageReceipt.toSendReply()

        logger.debug("Called send($request): $sendReply")
        return sendReply
    }
}

fun MessageReceipt.toSendReply(): SendReply {
    return SendReply.newBuilder()
        .setBody(this.body)
        .setId(this.id)
        .setPrice(this.price)
        .setRecipientNumber(this.recipientNumber)
        .setStatus(this.status)
        .build()
}
